//
//  HelloWorldSDK.h
//  HelloWorldSDK
//
//  Created by Rob Zmudzinski on 7/9/20.
//  Copyright © 2020 Rob Zmudzinski. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for HelloWorldSDK.
FOUNDATION_EXPORT double HelloWorldSDKVersionNumber;

//! Project version string for HelloWorldSDK.
FOUNDATION_EXPORT const unsigned char HelloWorldSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HelloWorldSDK/PublicHeader.h>


